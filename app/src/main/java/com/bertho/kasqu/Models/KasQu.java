package com.bertho.kasqu.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//klik kanan pada KasQu, constructor, pilih semua, tekan ok.
public class KasQu implements Serializable {
    @SerializedName("id")
    Integer id;

    @SerializedName("tanggal")
    String tanggal;

    @SerializedName("type")
    Integer type;

    @SerializedName("nominal")
    Integer nominal;

    @SerializedName("keterangan")
    String keterangan;

    @SerializedName("createdBy")
    String createdBy;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

//    public KasQu(String tanggal, Integer type, Integer nominal) {
//        this.tanggal = tanggal;
//        this.type = type;
//        this.nominal = nominal;
//    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }
}
