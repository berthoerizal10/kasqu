package com.bertho.kasqu.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable {
    @SerializedName("status") //yang ada di dalam kurung tidak boleh beda dengan key atau field di json
    boolean status;

    @SerializedName("message")
    String message;

    //cara membuat get set di java
    //klik kanan -> Getter -> Getter and Setter

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
