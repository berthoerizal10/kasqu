package com.bertho.kasqu.Api;

import com.bertho.kasqu.Models.KasQu;
import com.bertho.kasqu.Models.LoginResponse;
import com.bertho.kasqu.Models.SaveResponse;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEndPoint {

    //method login di api controller
    @POST("Login")
    @FormUrlEncoded
    Call<LoginResponse> doLogin(@Field("uname") String userName,
                                @Field("pwd") String password);

    @GET("GetListCatatan")
    Call<List<KasQu>> GetListCatatan();

    @POST("SimpanWithSp")
    Call<SaveResponse> simpan(@Body RequestBody obj);
}
