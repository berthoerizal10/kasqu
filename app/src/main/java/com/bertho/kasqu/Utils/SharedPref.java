package com.bertho.kasqu.Utils;

import android.content.Context;
import android.content.SharedPreferences;

// Untuk menyimpan session 
public class SharedPref {
    private Context context;
    private SharedPreferences sharedPreferences;
    static  final  String username = "userName";
    static  final  String isLogin = "isLogin";
    // jika ingin menambah field yang lain sesuaikan dg kebutuhan
    public SharedPref(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("KASQU_PREFS", Context.MODE_PRIVATE);
    }
    public void setUsername (String value) {
        sharedPreferences.edit().putString(username, value).apply();
    }
    public String getUsername() {
        return sharedPreferences.getString(username,"");
    }

    public void setIsLogin (boolean value) {
        sharedPreferences.edit().putBoolean(isLogin, value).apply();
    }
    public boolean getIsLogin() {
        return sharedPreferences.getBoolean(isLogin,false);
    }
}