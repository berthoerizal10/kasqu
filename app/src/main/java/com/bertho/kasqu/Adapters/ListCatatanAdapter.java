package com.bertho.kasqu.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.bertho.kasqu.Models.KasQu;
import com.bertho.kasqu.R;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ListCatatanAdapter extends ArrayAdapter<KasQu> {
    private Context context;
    private List<KasQu> kasQuList;
    public ListCatatanAdapter(@NonNull Context context, List<KasQu> listcatatan) {
        super(context, R.layout.item_list_catatan);
        this.kasQuList =listcatatan;
        this.context=context;
    }
    //klik kanan

    public int getCount(){
        return this.kasQuList.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.item_list_catatan,parent,false);

        TextView textViewTanggal = convertView.findViewById(R.id.tvTanggal);
        KasQu itemTanggal = this.kasQuList.get(position);
        textViewTanggal.setText(itemTanggal.getTanggal());

        TextView textViewNominal = convertView.findViewById(R.id.tvNominal);
        KasQu itemNominal = this.kasQuList.get(position);
        textViewNominal.setText((String.format("%,d", itemNominal.getNominal())).replace(',', '.'));

        TextView textViewCreatedBy = convertView.findViewById(R.id.tvCreatedBy);
        KasQu itemCreatedBy = this.kasQuList.get(position);
        textViewCreatedBy.setText(itemCreatedBy.getCreatedBy());

        TextView textViewType = convertView.findViewById(R.id.tvType);
        KasQu itemType = this.kasQuList.get(position);
        if(itemType.getType()==1){
            textViewType.setText("Pemasukkan");
            textViewType.setBackgroundResource(R.drawable.bg_pemasukan);
        } else {
            textViewType.setText("Pengeluaran");
            textViewType.setBackgroundResource(R.drawable.bg_pengeluaran);
        }

        // LocalDateTime dateTime = LocalDateTime.parse(itemTanggal.getTanggal());
        // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        // String dateTimeString = LocalDateTime.parse(itemTanggal.getTanggal().toString()).format(formatter);
        // textViewTanggal.setText(dateTimeString);

        return convertView;
    }
}
