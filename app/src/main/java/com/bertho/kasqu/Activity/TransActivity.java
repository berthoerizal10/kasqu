package com.bertho.kasqu.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bertho.kasqu.Api.ApiClient;
import com.bertho.kasqu.Api.ApiEndPoint;
import com.bertho.kasqu.Models.KasQu;
import com.bertho.kasqu.Models.SaveResponse;
import com.bertho.kasqu.R;
import com.bertho.kasqu.Utils.SharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    //deklarasi tanggal default
    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy");
    private String strDate = mdformat.format(calendar.getTime());
    private RadioButton rbPemasukkan,rbPengeluaran;
    private EditText etTTanggal,etNominal,etCatatan;
    private Button btnSave;
    private RadioGroup radioGroup;
    private KasQu kasQu;
    private boolean isEdit = false;
    private SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        sharedPref =new SharedPref(this);
        kasQu=(KasQu) getIntent().getSerializableExtra("model");
//        isEdit = getIntent().getBooleanExtra("edit", false);
        rbPemasukkan=findViewById(R.id.rbPemasukkan);
        rbPengeluaran=findViewById(R.id.rbPengeluaran);
        radioGroup = findViewById(R.id.groupTipe);
        btnSave = findViewById(R.id.btnSave);
        etNominal = findViewById(R.id.etNominal);
        etCatatan = findViewById(R.id.etCatatan);
        etTTanggal = findViewById(R.id.etTanggal);
        etTTanggal.setText(strDate);
        etTTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(TransActivity.this,
                        TransActivity.this, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpanData();
            }
        });

        if(kasQu!=null){
            isEdit=true;
            getData(kasQu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String sMonth = Integer.toString(month + 1);
        String sDay = Integer.toString(dayOfMonth);
        month++;
        if ((month + 1) < 10) {

            sMonth = "0" + month;
        }
        if (dayOfMonth < 10) {

            sDay = "0" + dayOfMonth;
        }

        @SuppressLint("DefaultLocale") String date = String.format("%s/%s/%d", sDay, sMonth, year);
        etTTanggal.setText(date);
    }

    void SimpanData(){
        int tipe =0;
        String tanggal;
        int nominal = 0;
        String catatan;

        //validasi
        if(etNominal.getText().toString().isEmpty()){
            etNominal.setError("Tentukan Nominal!");
            return;
        }
        if(etCatatan.getText().toString().isEmpty()){
            etNominal.setError("Tentukan KasQu!");
            return;
        }


        int id=radioGroup.getCheckedRadioButtonId();
        if(id==R.id.rbPemasukkan){
            tipe=1;
        }
        tanggal = etTTanggal.getText().toString();
        nominal=Integer.parseInt(etNominal.getText().toString());
        catatan = etCatatan.getText().toString();

        KasQu obj= new KasQu();
        //cek jika kondisi edit=true maka ambil id yang di parsing dari halaman beranda, jika false idnya jadikan NOl
        obj.setId(isEdit ? kasQu.getId():0);
        obj.setType(tipe);
        obj.setTanggal(tanggal);
        obj.setNominal(nominal);
        obj.setKeterangan(catatan);
        obj.setCreatedBy(sharedPref.getUsername());

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("Id", obj.getId().toString());
        builder.addFormDataPart("Tanggal", obj.getTanggal());
        builder.addFormDataPart("Type", obj.getType().toString());
        builder.addFormDataPart("Nominal", obj.getNominal().toString());
        builder.addFormDataPart("Keterangan", obj.getKeterangan());
        builder.addFormDataPart("CreatedBy", obj.getCreatedBy());
        RequestBody requestBody = builder.build();
        ApiEndPoint endPoint = ApiClient.getClient().create(ApiEndPoint.class);
        Call<SaveResponse> call = endPoint.simpan(requestBody);
        call.enqueue(new Callback<SaveResponse>() {
            @Override
            public void onResponse(Call<SaveResponse> call, Response<SaveResponse> response) {

                if(response.isSuccessful()){
                    new AlertDialog.Builder(TransActivity.this)
                            .setTitle("Info")
                            .setMessage("Berhasil Simpan")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            })
                            .setIcon(R.drawable.ic_action_success)
                            .show();
                }else {
                    new AlertDialog.Builder(TransActivity.this)
                            .setTitle("Error")
                            .setMessage("Gagal Simpan")
                            .setPositiveButton("OK",null)
                            .setIcon(R.drawable.ic_action_alert)
                            .show();
                }

            }

            @Override
            public void onFailure(Call<SaveResponse> call, Throwable t) {
                new AlertDialog.Builder(TransActivity.this)
                        .setTitle("Error")
                        .setMessage("Terjadi Kesalahan, coba lagi")
                        .setPositiveButton("OK",null)
                        .setIcon(R.drawable.ic_action_alert)
                        .show();
            }
        });
    }

    void getData(KasQu obj){
        int id=radioGroup.getCheckedRadioButtonId();
        if(obj.getType()==1){
            rbPemasukkan.setChecked(true);
        } else {
            rbPengeluaran.setChecked(true);
        }
        etTTanggal.setText(ConvertDate(obj.getTanggal(), "dd/MM/yyyy"));
        etNominal.setText(String.valueOf(obj.getNominal()));
        etCatatan.setText(String.valueOf(obj.getKeterangan()));
    }

    String ConvertDate(String date, String outputFormats) {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat = new SimpleDateFormat(outputFormats);

        return timeFormat.format(myDate);

    }
}