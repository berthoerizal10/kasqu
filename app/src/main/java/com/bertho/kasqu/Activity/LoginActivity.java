package com.bertho.kasqu.Activity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bertho.kasqu.Api.ApiClient;
import com.bertho.kasqu.Api.ApiEndPoint;
import com.bertho.kasqu.Models.LoginResponse;
import com.bertho.kasqu.R;
import com.bertho.kasqu.Utils.SharedPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    EditText etUsername,etPassword;
    String result;
    private ProgressDialog progressDialog;
    private SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref=new SharedPref(this);
        //start hide action bar
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        //end hide action bar

        setContentView(R.layout.activity_login);
        progressDialog = new ProgressDialog(this);

        //start logic here
        btnLogin = findViewById(R.id.btnLogin);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // String result = "Username: "+etUsername.getText().toString()+"\n Password: "+etPassword.getText().toString();
                // Toast.makeText(LoginActivity.this, result, Toast.LENGTH_SHORT).show();
                // Integer checkUsername = Integer.valueOf(etUsername.getText().length());
                //  Integer checkPassword = Integer.valueOf(etPassword.getText().length());

                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                func_loginToApp(username,password);
            }
        });
    }

    void func_loginToApp(String checkUsername, String checkPassword){
        if(checkUsername.isEmpty()){
            func_alert("Username tidak boleh kosong");
            return;
        }
        if(checkPassword.isEmpty()){
            func_alert("Password tidak boleh kosong");
            return;
        }

        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiEndPoint endPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<LoginResponse> call = endPoint.doLogin(checkUsername, checkPassword);
        call.enqueue(new Callback<LoginResponse>() {
            //
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                //parsing json api ke data model LoginResponse
                LoginResponse data = response.body();
                if(data.isStatus()){
                    sharedPref.setIsLogin(true);
                    sharedPref.setUsername(checkUsername);
                    func_alertSuccess(data.getMessage());
                    Intent intent = new Intent(LoginActivity.this,BerandaActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    func_alert(data.getMessage());
                }
            }

            //ini untuk menangkap error, misal koneksi terputus dll.
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                func_alert("Terjadi kesalahan, coba lagi!");
            }
        });
    }

    void func_alert(String msg){
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .setIcon(R.drawable.ic_action_alert)
                .show();
    }

    void func_alertSuccess(String msg){
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Success")
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .setIcon(R.drawable.ic_action_success)
                .show();
    }
}