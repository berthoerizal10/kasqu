package com.bertho.kasqu.Activity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.bertho.kasqu.R;
import com.bertho.kasqu.Utils.SharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {
    //deklarasi variable global
    String nama,pendidikan,alamat,ttl;
    final String nama1="Final tidak bisa dirubah";
    int satu = 0;
    boolean salah = false;
    List<String> list= new ArrayList<String>();

    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //hide action bar
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        // end hide action bar

        setContentView(R.layout.activity_splash_screen);
        sharedPref = new SharedPref(this);
        openLoginForm();
    }

    //perintah pindah halaman setelah 3 detik
    void openLoginForm(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if(sharedPref.getIsLogin()){
                    Intent intent = new Intent(SplashScreenActivity.this,BerandaActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashScreenActivity.this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        };

        new Timer().schedule(task,2000);
    }
}