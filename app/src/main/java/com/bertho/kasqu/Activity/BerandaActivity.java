package com.bertho.kasqu.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bertho.kasqu.Adapters.ListCatatanAdapter;
import com.bertho.kasqu.Api.ApiClient;
import com.bertho.kasqu.Api.ApiEndPoint;
import com.bertho.kasqu.Models.KasQu;
import com.bertho.kasqu.R;
import com.bertho.kasqu.Utils.SharedPref;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BerandaActivity extends AppCompatActivity implements AdapterView.OnItemClickListener { // implements adapterview.onitemclicklistener => untuk event listview diklik
    List<KasQu> kasQuList = new ArrayList<>();
    ListCatatanAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ListView listView;
    private FloatingActionButton btnAdd;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);
        sharedPref = new SharedPref(this);
        listView = findViewById(R.id.listview);
        listView.setOnItemClickListener(this);
        swipeRefreshLayout=findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromApi();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        getDataFromApi();

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BerandaActivity.this,TransActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // untuk mengambil layout menu supaya nampil di pojok kanan atas
        getMenuInflater().inflate(R.menu.menu_logout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //ini untuk handle ketika menu di pojok kanan atas di klik
        int id = item.getItemId();
        if(id==R.id.action_logout){
            new AlertDialog.Builder(BerandaActivity.this)
                    .setTitle("Logout")
                    .setMessage("Yakin ingin logout?")
                    .setNegativeButton("Batal", null)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            sharedPref.setIsLogin(false);
                            finish();
                        }
                    })
                    .setIcon(R.drawable.ic_action_alert)
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        getDataFromApi();
        super.onResume();
    }

    void getDataFromApi(){
        ApiEndPoint endPoint= ApiClient.getClient().create(ApiEndPoint.class);
        Call<List<KasQu>> call = endPoint.GetListCatatan();
        call.enqueue(new Callback<List<KasQu>>() {
            @Override
            public void onResponse(Call<List<KasQu>> call, Response<List<KasQu>> response) {
                kasQuList.clear();
                kasQuList.addAll(response.body());
                adapter = new ListCatatanAdapter(BerandaActivity.this, kasQuList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<KasQu>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        //parsing data model ke form Transactivity,
        //parsign bisa saja denga tipe data seperti string, int dll
        // contoh parsing data string:
        //intent.putS
        KasQu obj = kasQuList.get(position);
        Intent intent = new Intent(this, TransActivity.class);
        intent.putExtra("model",obj);
        intent.putExtra("edit",true);
        startActivity(intent);
    }
}