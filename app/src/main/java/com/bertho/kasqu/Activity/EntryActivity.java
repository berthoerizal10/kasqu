package com.bertho.kasqu.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bertho.kasqu.R;

public class EntryActivity extends AppCompatActivity {
    Button btn; //public variable
    EditText text_name;
    TextView tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);

        //start code here
        btn = findViewById(R.id.btnSubmit);
        text_name = findViewById(R.id.etNama);
        tvNama = findViewById(R.id.tvNamaSaya);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addName();
            }
        });
        //end code
    }

    void addName(){
        tvNama.setText(text_name.getText().toString());
        Toast.makeText(EntryActivity.this,"Nama kamu: "+text_name.getText().toString(),Toast.LENGTH_SHORT).show();
    }
}